﻿using log4net.Core;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using log4net.Layout;
using log4net.Util;
using System.Net;

namespace log4net.Appender
{
    public class SocketAppender : AppenderSkeleton
    {
        public SocketAppender()
        {
            AddressFamily = AddressFamily.InterNetwork;
            SocketType = SocketType.Stream;
            ProtocolType = ProtocolType.Tcp;
            ConAttemptsCount = 3;
            ConAttemptsWaitingTimeMilliSeconds = 1000;
            UseThreadPoolQueue = false;
        }
        public IPAddress RemoteAddress { get; set; }
        public int RemotePort { get; set; }
        public bool DebugMode { get; set; }
        public AddressFamily AddressFamily { get; set; }
        public SocketType SocketType { get; set; }
        public ProtocolType ProtocolType { get; set; }
        public int ConAttemptsCount { get; set; }
        public int ConAttemptsWaitingTimeMilliSeconds { get; set; }
        public bool UseThreadPoolQueue { get; set; }
        public int ReconnectTimeInSeconds { get; set; }

        private static DateTime? _nextTrialTime = null;
        private Socket _socket;

        public override void ActivateOptions()
        {
            if (_nextTrialTime.HasValue && _nextTrialTime.Value > DateTime.Now) return;
            else _nextTrialTime = null;
            
            var retryCount = 0;
            while (++retryCount <= ConAttemptsCount)
            {
                try
                {
                    _socket = new Socket(AddressFamily, SocketType, ProtocolType);
                    _socket.Connect(RemoteAddress, RemotePort);
                    break;
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ErrorHandler.Error(string.Format("ArgumentNullException : {0}", argumentNullException), argumentNullException, ErrorCode.GenericFailure);
                }
                catch (SocketException socketException)
                {
                    ErrorHandler.Error(string.Format("SocketException : {0}", socketException), socketException, ErrorCode.GenericFailure);
                }
                catch (Exception exception)
                {
                    ErrorHandler.Error("Could not initialize the TcpSocket for remote host " + this.RemoteAddress.ToString() + " on port " + this.RemotePort + ".", exception, ErrorCode.GenericFailure);
                }
                Thread.Sleep(ConAttemptsWaitingTimeMilliSeconds);
            }
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            if (UseThreadPoolQueue)
                ThreadPool.QueueUserWorkItem(state => AppendLog(loggingEvent));
            else
                AppendLog(loggingEvent);

        }

        private void AppendLog(LoggingEvent loggingEvent)
        {
            string rendered = string.Empty;

            if (_socket.Connected)
            {
                rendered = RenderLoggingEvent(loggingEvent);
                Byte[] buffer = Encoding.Unicode.GetBytes(rendered.ToCharArray());

                try
                {
                    var bytesSent = _socket.Send(buffer);
                    
                    if (DebugMode)
                    {
                        Console.WriteLine("- Bytes sent: " + bytesSent);
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    ErrorHandler.Error(string.Format("ArgumentNullException : {0}", argumentNullException), argumentNullException, ErrorCode.GenericFailure);
                }
                catch (SocketException socketException)
                {
                    ErrorHandler.Error(string.Format("SocketException : {0}", socketException), socketException, ErrorCode.GenericFailure);
                }
                catch (Exception exception)
                {
                    ErrorHandler.Error("Unable to send logging event to remote host " + this.RemoteAddress.ToString() + " on port " + this.RemotePort + ".", exception, ErrorCode.WriteFailure);
                }
            }
            else
            {
                ErrorHandler.Error("[UNSUCCESSFULL]:: " + rendered);

                if (!_nextTrialTime.HasValue)
                {
                    _nextTrialTime = DateTime.Now.AddSeconds(ReconnectTimeInSeconds);
                }
                ActivateOptions();
            }
        }

        override protected bool RequiresLayout
        {
            get { return true; }
        }

        protected override void OnClose()
        {
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
        }
    }
}

